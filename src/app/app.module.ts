import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CandidateComponent } from './candidate/candidate.component';
import { JobComponent } from './job/job.component';
import { ApplicationComponent } from './application/application.component';
import { HttpClientModule } from '@angular/common/http';
import {HttpService} from './http.service';
import { ApplyJobComponent } from './apply-job/apply-job.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateJobComponent } from './create-job/create-job.component';

@NgModule({
  declarations: [
    AppComponent,
    CandidateComponent,
    JobComponent,
    ApplicationComponent,
    ApplyJobComponent,
    CreateJobComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [HttpService],
  bootstrap: [AppComponent]
})
export class AppModule { }
