import {JobSkillWeightageRequired} from '../models/job-skill-weightage-required.model';
export class Job {
    public jobId : number;
    public jobTitle : string; 
    public jobCode : string ;
    public description : string;
    public createdDate : string;
    public expiryDate : string;
    public status : string;
    public jobSkillWeightageRequired: JobSkillWeightageRequired[];
}
