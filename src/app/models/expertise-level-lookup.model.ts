export class ExpertiseLevelLookup {
    public expertiseLevelId: number;
    public expertiseLevel: string;
    public score: number;
}
