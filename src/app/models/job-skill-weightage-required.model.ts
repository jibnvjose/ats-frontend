import {SkillSet} from '../models/skill-set.model'
export class JobSkillWeightageRequired {
    public jobSkillWeightageRequiredId : number;
    public jobID : number;
    public skillID : number;
    public expertiseLevelID : number;
    public minimumYearsOfExperienceRequired : number;
    public maximumYearsOfExperienceRequired : number;
    public isMandatory : CharacterData;
    public weightage : number;
    public  skill : SkillSet
}
