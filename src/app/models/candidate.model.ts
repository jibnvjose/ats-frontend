import {JobApplication} from '../models/job-application.model';
export class Candidate {
    public candidateId : number;
    public firstName : string;
    public lastName : string;
    public middleName: string;
    public dateOfBirth: string;
    public totalYearsOfExperience: number;
    public employmentStatus: string;
    public current_employer: string;
    public jobApplication: JobApplication;
 
    constructor(candidateId : number, firstName : string, lastName : string, middleName:string,
         dateOfBirth:string, totalYearsOfExperience:number, employmentStatus:string,
         current_employer:string){
        this.candidateId = candidateId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.dateOfBirth = dateOfBirth;
        this.totalYearsOfExperience = totalYearsOfExperience;
        this.employmentStatus = employmentStatus;
        this.current_employer = current_employer;
    } 
}