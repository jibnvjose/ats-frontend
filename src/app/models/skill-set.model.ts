export class SkillSet {
    public skillId: number;
    public description: string;
    public isEnabled: number;
}
