import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from '../http.service';
import { JobSkillWeightageRequired } from '../models/job-skill-weightage-required.model';
import { Job } from '../models/job.model';
import { ExpertiseLevelLookup } from '../models/expertise-level-lookup.model';

@Component({
  selector: 'app-apply-job',
  templateUrl: './apply-job.component.html',
  styleUrls: ['./apply-job.component.css']
})
export class ApplyJobComponent implements OnInit {
  jobID: string;
  job: Job;
  expertiseLevels: ExpertiseLevelLookup[];
  form: FormGroup;

  constructor(private route: ActivatedRoute, private httpService: HttpService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      middleName: new FormControl('', Validators.required),
      jobSkillWeightageRequired: this.formBuilder.array([this.createSkillSetRequiredFormGroup()])
    });
    this.route.queryParams.subscribe(
      params => {
        this.jobID = params['jobID'];
      }
    )

    this.httpService.sendGetRequest('/api/job/' + this.jobID).subscribe((job: Job) => {
      this.job = job;
      console.log(this.job);

      this.httpService.sendGetRequest('/api/expertiseLevels').subscribe((expertiseLevels: ExpertiseLevelLookup[]) => {
        this.expertiseLevels = expertiseLevels;
        console.log(this.expertiseLevels);
      });

    });

  }

  get f() {
    return this.form.controls;
  }

  submit() {
    console.log(this.form.value);
    // this.postService.create(this.form.value).subscribe(res => {
    //      console.log('Post created successfully!');
    //      this.router.navigateByUrl('post/index');
    // })
  }

  private createSkillSetRequiredFormGroup(): FormGroup {
    return new FormGroup({
      'skill': new FormControl(0, Validators.required)
    })
  }

}
