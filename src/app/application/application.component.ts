import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { JobApplication } from '../models/job-application.model';

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.css']
})
export class ApplicationComponent implements OnInit {
  jobApplications: JobApplication[];
  constructor(private httpService: HttpService) {
    this.jobApplications = [];
  }

  ngOnInit(): void {
    this.httpService.sendGetRequest('/api/jobApplications').subscribe((jobApplications: JobApplication[]) => {
      this.jobApplications = jobApplications;
      console.log(this.jobApplications);
    });
  }

}
