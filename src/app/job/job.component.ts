import { Component, OnInit } from '@angular/core';
import { Job } from '../models/job.model';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.css']
})
export class JobComponent implements OnInit {
  jobs: Job[];
  constructor(private httpService: HttpService) {
    this.jobs = [];
  }

  ngOnInit(): void {
    this.httpService.sendGetRequest('/api/job').subscribe((jobs: Job[]) => {
      this.jobs = jobs;
      console.log(this.jobs);
    });
  }

}
