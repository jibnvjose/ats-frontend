import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { SkillSet } from '../models/skill-set.model';
import { HttpService } from '../http.service';
import { ExpertiseLevelLookup } from '../models/expertise-level-lookup.model';

@Component({
  selector: 'app-create-job',
  templateUrl: './create-job.component.html',
  styleUrls: ['./create-job.component.css']
})

export class CreateJobComponent implements OnInit {
  form: FormGroup;
  skillSets: SkillSet[];
  expertiseLevels: ExpertiseLevelLookup[];
  constructor(private formBuilder: FormBuilder, private httpService: HttpService) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      jobTitle: new FormControl('', Validators.required),
      jobCode: new FormControl(''),
      description: new FormControl('', Validators.required),
      expiryDate: new FormControl(''),
      status: new FormControl(0),
      jobSkillWeightageRequired: this.formBuilder.array([this.createSkillSetRequiredFormGroup()])
    });

    this.httpService.sendGetRequest('/api/skillSets').subscribe((skillSets: SkillSet[]) => {
      this.skillSets = skillSets;
      console.log(this.skillSets);
    });

    this.httpService.sendGetRequest('/api/expertiseLevels').subscribe((expertiseLevels: ExpertiseLevelLookup[]) => {
      this.expertiseLevels = expertiseLevels;
      console.log(this.expertiseLevels);
    });

  }

  get f() {
    return this.form.controls;
  }

  submit() {
    console.log(this.form.value);
    // this.postService.create(this.form.value).subscribe(res => {
    //      console.log('Post created successfully!');
    //      this.router.navigateByUrl('post/index');
    // })
  }
  private createSkillSetRequiredFormGroup(): FormGroup {
    return new FormGroup({
      'skill': new FormControl(0, Validators.required),
      'expertiseLevel': new FormControl(0),
      'weightage': new FormControl('1.0', Validators.required),
      'isMandatory': new FormControl('')
    })
  }
  public addSkillSetRequiredFormGroup() {
    const jobSkillWeightageRequired = this.form.get('jobSkillWeightageRequired') as FormArray
    jobSkillWeightageRequired.push(this.createSkillSetRequiredFormGroup())
  }
  public removeOrClearSkillSetRequired(i: number) {
    const jobSkillWeightageRequired = this.form.get('jobSkillWeightageRequired') as FormArray
    if (jobSkillWeightageRequired.length > 1) {
      jobSkillWeightageRequired.removeAt(i)
    } else {
      jobSkillWeightageRequired.reset()
    }
  }
}
