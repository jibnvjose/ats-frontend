import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  
  apiUrl = "https://localhost:44371";
  constructor(private httpClient: HttpClient) { }
  
  sendGetRequest(endpoint) {
    return this.httpClient.get(this.apiUrl + endpoint);
  }
  sendPostRequest(endpoint, model) {
    return this.httpClient.post(this.apiUrl + endpoint,model);
  }
}
