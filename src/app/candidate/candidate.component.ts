import { Component, OnInit } from '@angular/core';
import { Candidate } from '../models/candidate.model';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-candidate',
  templateUrl: './candidate.component.html',
  styleUrls: ['./candidate.component.css']
})
export class CandidateComponent implements OnInit {

  candidates: Candidate[];

  constructor(private httpService: HttpService) {
    this.candidates = [];
  }

  ngOnInit(): void {
    this.httpService.sendGetRequest('/api/candidates').subscribe((candidates: Candidate[]) => {
      this.candidates = candidates;
      console.log(this.candidates);
    });
  }

}
