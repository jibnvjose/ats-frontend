import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CandidateComponent } from './candidate/candidate.component';
import {JobComponent} from './job/job.component';
import {ApplicationComponent} from './application/application.component';
import { ApplyJobComponent } from './apply-job/apply-job.component';
import { CreateJobComponent } from './create-job/create-job.component';


const routes: Routes = [
  { path: 'candidates', component: CandidateComponent },
  { path: 'jobs', component: JobComponent},
  { path: 'applications', component: ApplicationComponent},
  { path: 'apply-job', component: ApplyJobComponent},
  { path: 'create-job', component: CreateJobComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
